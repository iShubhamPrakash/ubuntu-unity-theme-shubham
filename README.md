# ubuntu-unity-theme-shubham
An updated theme for ubuntu unity desktop environment.</br>
Hello unity DE lover. This theme is for those who want to have mac-live desktop on there ubuntu.</br>
Instructions to install mac-like theme on your ubuntu.</br>
NOTE: You must be running unity destop ontu your ubuntu operating system.</br></br>

step 1: Download the ubuntu-unity-theme-shubham.zip file.</br>
step 2: unzip the ubuntu-unity-theme-shubham.zip file. After you extract the zip file, you will see a folder having </br>
        name "ubuntu-unity-theme-shubham.zip" containing couple more folders which have source for the theme.</br>
step 3: Now copy this folder (ubuntu-unity-theme-shubham.zip) to the derectory (/usr/local/share/themes) manually or</br>
        copy this command inside the double quotes and run in the terminal-</br>
        "mv ubuntu-unity-theme-shubham /usr/local/share/themes"</br>
        
step 4: Now open unity tweak tool and select the "ubuntu-unity-theme-shubham" in the theme section</br>
(If you have not installed unity tweak tool the run this command in a terminal window- </br>
"sudo apt get install unity-tweak-tool") </br>

Thanks and have fun :) </br>
Shubham Prakash </br>
https://www.facebook.com/i-suvm
